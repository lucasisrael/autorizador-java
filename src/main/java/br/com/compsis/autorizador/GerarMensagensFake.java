/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/01/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador;

import javax.jms.BytesMessage;
import javax.jms.JMSException;

import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.Tag;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.Tag.Builder;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.TagEntrada;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.TagEntrada.Categoria;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.TagEntrada.Grupo;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.TagEntrada.Plano;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.TagEntrada.Situacao;

import com.google.common.base.Strings;

import br.com.compsis.autorizador.core.jms.JMSConnection;
import br.com.compsis.autorizador.core.jms.JMSConnection.ProdutorBuilder;
import br.com.compsis.autorizador.core.jms.JMSConnectionUtil;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/01/2015 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class GerarMensagensFake {
	public static void main(String[] args) throws JMSException {
		JMSConnection connection = JMSConnectionUtil.criarJMSConnection("CONCENTRADOR", "tags.PR78V07", false);
		
		for (int i = 1; i < 3000; i++) {
			ProdutorBuilder<BytesMessage> produtorBuilder = connection.criarByteMessage();
			Builder builder = Tag.newBuilder();
			builder.setOsaId(4).setConcessionariaId(1005).setSerie(1).setSequencial(i);
			for (int j = 0; j < 10; j++) {
				builder.addTag(TagEntrada.newBuilder().setCategoria(Categoria.CAT01)
						.setGrupo(Grupo.ISENTO_ARTESP)
						.setPlaca("DFZ"+Strings.padStart(String.valueOf(j), 4, '0'))
						.setPlano(Plano.POS_PAGO)
						.setSituacao(Situacao.OK)
						.setTagId( Long.valueOf( String.valueOf(i)+j ) * (i*200) ) 
						.setTemPassagem(false)
						.setVencimento(15)
						);
				
			}
			
			produtorBuilder.getMessage().writeBytes(builder.build().toByteArray());
			produtorBuilder.enviar();
			System.out.println("Enviado mensagem seq "+i);
		}
		
		connection.fecharTudo();
		System.out.println("Concluido!");
	}
}
