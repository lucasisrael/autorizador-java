
package br.com.compsis.autorizador;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.compsis.autorizador.core.dao.sql.SQLiteConnection;
import br.com.compsis.autorizador.core.dao.xmls.ConfiguracaoAutorizadorDAO;
import br.com.compsis.autorizador.core.facade.AutorizadorFacade;
import br.com.compsis.autorizador.core.jmx.RegistradorJMX;
import br.com.compsis.autorizador.domain.Autorizador;



@SuppressWarnings("unchecked")
public final class Util {
	private Util(){}
	private static final ClassLoader CLASS_LOADER = Util.class.getClassLoader();
	
	public final static String AUTORIZADOR_HOME;
	private static Logger log = LoggerFactory.getLogger(Util.class);
	private static final Map<String, Object> containerObjetos = Collections.synchronizedMap(new HashMap<String, Object>());
	
	static {
		AUTORIZADOR_HOME = Util.CARREGAR_VARIAVEL_AMBIENTE("AUTORIZADOR_HOME", new File(new File("D:", "sicat"), "autorizador").getAbsolutePath());
		registrarBean(ConfiguracaoAutorizadorDAO.class.getName(), new ConfiguracaoAutorizadorDAO());
		registrarBean(RegistradorJMX.class.getName(), new RegistradorJMX());
	}
	
	/**
	 * Obtem instancia do objeto com o nome informado 
	 * @param name
	 * @return
	 */
	public static <B> B getBean(String name) {
		synchronized (containerObjetos) {
			Object object = containerObjetos.get(name);
			return (B) object;			
		}
	}
	
	public static SQLiteConnection getConexao(Autorizador autorizador) {
		String chave = "DB_" + AUTORIZADOR_HOME + autorizador.getCodigo();
		SQLiteConnection conexao = getBean(chave);
		if(conexao == null) {
			
			conexao = new SQLiteConnection().setArquivoDB(new File(new File(AUTORIZADOR_HOME, autorizador.getCodigo()), "Autorizador.DB").getAbsolutePath());
			registrarBean(chave, conexao);
		}
		return conexao.conectar();
	}
	
	
	public synchronized static AutorizadorFacade getAutorizadorFacade(Autorizador autorizador) {
		String chave = "AutorizadorFacade_" + AUTORIZADOR_HOME + autorizador.getCodigo();
		AutorizadorFacade facade = getBean(chave);
		if(facade == null) {
			facade = new AutorizadorFacade(autorizador);
			registrarBean(chave, facade);
		}
		
		return facade;
	}
	
	public static void atualizarClassLoader() {
		Thread.currentThread().setContextClassLoader(CLASS_LOADER);
	}
	
	/**
	 * Obtem instancia do objeto para a classe informada
	 * @param name
	 * @return
	 */
	public static <B> B getBean(Class<B> classe) {
		return getBean(classe.getName());
	}
	
	/**
	 * Registra um novo objeto no container
	 * @param nome
	 * @param objeto
	 */
	public static void registrarBean (String nome, Object objeto) {
		synchronized (containerObjetos) {
			containerObjetos.put(nome, objeto);			
		}
	}
	
	
	/**
	 * Concatena as {@link String} informada pelo parametro utilizando {@link StringBuffer}
	 * @param args
	 * @return
	 */
	public static String concat(Object...args) {
		StringBuffer buffer = new StringBuffer();
		for (Object string : args) {
			buffer.append(string);
		}
		return buffer.toString();
	} 
	/**
	 * Carrega variavel de ambiente. Retorna o valor default caso nao encontre
	 * @param nomeVariavel
	 * @param valorParaNulo
	 * @return
	 */
	public static String CARREGAR_VARIAVEL_AMBIENTE(String nomeVariavel, String valorParaNulo) {
		if(log.isDebugEnabled()) {
			log.debug(concat("Buscando variavel de ambiente ", nomeVariavel, ". Se nao encontrado, usara valor ", valorParaNulo));			
		}
		String retorno = System.getenv(nomeVariavel);
		if(retorno == null) {
			log.warn(concat("Variavel de ambiente ", nomeVariavel, " nao definida. Utilizando valor ", valorParaNulo));
		}
		return retorno == null ? valorParaNulo : retorno;
	}
}
