/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 24/01/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador.core.dao.xmls;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.compsis.autorizador.Util;
import br.com.compsis.autorizador.domain.Autorizador;
import br.com.compsis.autorizador.domain.ConfiguracaoAutorizador;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;


public class ConfiguracaoAutorizadorDAO {
	private XStream xstream;
	private static Logger log = LoggerFactory.getLogger(ConfiguracaoAutorizadorDAO.class);
	private String caminhoArquivoXML;
	
	private synchronized XStream getXStream() {
		if(xstream == null) {
			xstream = new XStream(new PureJavaReflectionProvider());
			xstream.alias("configuracoes", ConfiguracaoAutorizador.class);
			xstream.alias("autorizador", Autorizador.class);
		}
		return xstream;
	}
	

	public synchronized String getArquivoConfiguracao() {
		if(caminhoArquivoXML == null) {
			caminhoArquivoXML = new File(Util.AUTORIZADOR_HOME, "ConfiguracaoAutorizador.xml").getAbsolutePath();			
		}
		return caminhoArquivoXML;
	}
	
	
	
	public ConfiguracaoAutorizador carregarConfiguracaoAutorizador() {
		ConfiguracaoAutorizador configuracao = null;
		try {
			ObjectInputStream objInputStream = getXStream().createObjectInputStream(new FileReader(new File(getArquivoConfiguracao())));
			configuracao = (ConfiguracaoAutorizador) objInputStream.readObject();
		} catch (Exception e) {
			log.info(e.getMessage());
			log.debug(e.getMessage(), e); 
		}
		
		return configuracao;
	}
	
	
	public void escreverConfiguracaoAutorizador(final ConfiguracaoAutorizador configuracao) {
		ObjectOutputStream objOutputStream = null;
		try {
			objOutputStream = getXStream().createObjectOutputStream(new FileWriter(new File(getArquivoConfiguracao())));
			objOutputStream.writeObject(configuracao);
		} catch (Exception e) {
			log.info(e.getMessage());
			log.debug(e.getMessage(), e); 
		} finally {
			if(objOutputStream!=null) {
				try {
					objOutputStream.flush();
					objOutputStream.close();
				} catch (IOException e) {
					log.info(e.getMessage());
					log.debug(e.getMessage(), e); 
				}
			}
		}
	}
}
