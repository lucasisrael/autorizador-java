/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/01/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador.core.jms;

import javax.jms.BytesMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.Tag;

import br.com.compsis.autorizador.Conexoes;
import br.com.compsis.autorizador.core.midia.RecebedorMidia;
import br.com.compsis.autorizador.domain.Autorizador;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/01/2015 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class TagConsumer implements MessageListener {
	private static Logger log = LoggerFactory.getLogger(TagConsumer.class);
	private RecebedorMidia recebedor;
	private Autorizador autorizador;
	private String codigo;
	
	public TagConsumer(Autorizador autorizador) {
		recebedor = new RecebedorMidia(autorizador);
		codigo = autorizador.getCodigo();
		this.autorizador = autorizador;
	}
	
	
	@Override
	public void onMessage(Message message) {
		BytesMessage byteMessage = (BytesMessage) message;
		try {
			byte[] bytes = new byte[(int) byteMessage.getBodyLength()];
			byteMessage.readBytes(bytes);
			Tag tag = Tag.newBuilder().mergeFrom(bytes).build();
			recebedor.processarTag(tag);
		} catch (Exception e) {
			log.error("Erro do log: ------------------------>>>>>>>>>>>>"+e.getMessage(), e);
			log.debug(e.getMessage(), e);
			Conexoes.addAutorizador(this.autorizador);
		}
	}

}
