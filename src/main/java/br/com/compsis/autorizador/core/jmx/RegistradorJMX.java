/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/01/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador.core.jmx;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.List;

import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.StandardMBean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.compsis.autorizador.domain.Autorizador;
import br.com.compsis.autorizador.domain.MOP;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/01/2015 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class RegistradorJMX {
	private static Logger log = LoggerFactory.getLogger(RegistradorJMX.class);
	public final static String MAIN_PACKAGE = "br.com.compsis.autorizador";
	public final static String AUTORIZADOR_JMX_NAME = MAIN_PACKAGE+":group=Autorizadores,";
	private static MBeanServer mBeanServer;
	private static List<Object> mbeans = new ArrayList<Object>();
	
	public static String getNomeJMX(Autorizador autorizador) {
		return AUTORIZADOR_JMX_NAME + "type=Geral,name=Autorizador_" + autorizador.getCodigo();
	}

	public static String getNomeJMX(Autorizador autorizador, MOP mop) {
		return AUTORIZADOR_JMX_NAME + "type=Detalhado,name=" + autorizador.getCodigo()+"_OSA"+mop.getOsaId();
	}

	
	public void registrarAutorizador(Autorizador autorizador) {
		try {
			validateMBeanServer();
			AutorizadorMBean autorizadorMBean = new AutorizadorMBean(autorizador); 
			ObjectName objectName = new ObjectName(getNomeJMX(autorizador));
			StandardMBean standardMBean = new StandardMBean(autorizadorMBean, IAutorizadorMBean.class);
			ObjectInstance objectInstance = mBeanServer.registerMBean(standardMBean, objectName);
			mbeans.add(objectInstance);
			
			List<MOP> mops = autorizador.listMOPs();
			for (MOP mop : mops) {
				MOPMBean mopmBean = new MOPMBean(autorizador, mop);
				objectName = new ObjectName(getNomeJMX(autorizador, mop));
				standardMBean = new StandardMBean(mopmBean, IMOPMBean.class);
				objectInstance = mBeanServer.registerMBean(standardMBean, objectName);
				mbeans.add(objectInstance);
			}
		} catch (Exception e) {
			log.info(e.getMessage());
			log.debug(e.getMessage(), e); 
		} 
		
	}
	
	private synchronized void validateMBeanServer() {
		if(mBeanServer==null) {
			log.debug("Obtendo MBeanServer...");
			mBeanServer = ManagementFactory.getPlatformMBeanServer();
			log.debug("MBean Server obtido");			
		}
	}
}
