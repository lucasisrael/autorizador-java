/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/12/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador.core.jms;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import br.com.compsis.autorizador.core.jms.JMSConnection.ProdutorBuilder;


/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/12/2014 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class EnviarReceberMensagem {
	final static Scanner scan = new Scanner(System.in);
	final static Map<String, JMSConnection> CONN = new HashMap<String, JMSConnection>();
	final static String HOST = "CONCENTRADOR";
	static boolean HELP  = true;
	
	
	public static void main(String[] args) throws JMSException {	
		perguntar();
	}
	
	
	
	public static void perguntar() throws JMSException {
		imprimirMensagem();
		String opcao = scan.nextLine();
		if(opcao!=null) {
			String[] input = opcao.trim().split("-");
			String cmd = input[0].trim();
			if(cmd.trim().toLowerCase().equals("help")) {
				HELP = true;
			} else if(cmd.trim().toLowerCase().equals("ex")) { 
				for (Entry<String, JMSConnection> entry : CONN.entrySet()) {
					System.out.println("Fechando "+entry.getKey());
					entry.getValue().fecharTudo();
				}
				CONN.clear();
				System.out.println("Fechou todas as conexoes abertas. Saindo!");
				return;
			} else {
				String chave = criarChave(HOST, cmd);
				
				int qtdMensagens = 1;
				
				JMSConnection jmsConnection = CONN.get(chave);
				if(jmsConnection == null) {
					jmsConnection = JMSConnectionUtil.criarJMSConnection(HOST, cmd, false);
					System.out.println("Criado nova conexao: "+HOST+" fila: "+cmd);
					CONN.put(chave, jmsConnection);
				}
				for (int i = 1; i < input.length; i++) {
					if(input[i].toLowerCase().trim().equals("fcs")) {
						jmsConnection.fecharConsumidor();
						System.out.println("Consumidor fechado!");
					} else if(input[i].toLowerCase().trim().equals("dm")) {
						JMSConnection.showDetailedMessage();
					} else if(input[i].toLowerCase().trim().equals("ndm")) {
						JMSConnection.showShortMessage();						
					} else if(input[i].toLowerCase().trim().equals("fcx")) {
						jmsConnection.fecharConexao();
						System.out.println("Conexao fechada");
					} else if(input[i].toLowerCase().trim().equals("ft")) {
						jmsConnection.fecharTudo();
						CONN.remove(chave);
						System.out.println("Fechando tudo da conexao: "+HOST+" fila: "+cmd);
					} else if(input[i].toLowerCase().trim().equals("fp")) {
						jmsConnection.fecharProdutor();
						System.out.println("Produtor fechado!");
					} else if(input[i].toLowerCase().trim().startsWith("em=")) {
						if(opcao.toLowerCase().contains("-qtd=")) {
							for (String cmdQtd : input) {
								if(cmdQtd.toLowerCase().trim().startsWith("qtd=")){
									String qtd = cmdQtd.substring(cmdQtd.indexOf("=")+1);
									qtdMensagens = Integer.valueOf(qtd);
								}
							}
						}
						String[] envioMSG = input[i].split("=");
						if(envioMSG.length!=2) {
							System.err.println("Erro. Envio de mensagem no formato invalido. Uso correto: EM=[mensagem aqui]");
						} else {
							for (int j = 0; j < qtdMensagens; j++) {
								enviarMensagemTexto(jmsConnection, envioMSG[1]+" - MSG numero: "+j);								
							}
						}
					} else if(input[i].toLowerCase().trim().equals("ic")) {
						receberMensagemTexto(jmsConnection, cmd);
						System.out.println("Consumidor iniciado!");
					} else if(input[i].toLowerCase().trim().equals("ex")) {
						for (Entry<String, JMSConnection> entry : CONN.entrySet()) {
							System.out.println("Fechando "+entry.getKey());
							entry.getValue().fecharTudo();
						}
						CONN.clear();
						System.out.println("Fechou todas as conexoes abertas. Saindo!");
						return;
					} else {
						System.out.println("Comando desconhecido: "+input[i]);
					}
				}
			}
		}
		perguntar();
	}
	
	private static String criarChave(String...valores) {
		StringBuilder builder = new StringBuilder();
		for (String string : valores) {
			builder.append(string);
		}
		return builder.toString();
	}
	
	public static void imprimirMensagem() {
		StringBuilder builder = new StringBuilder();
		final String LINE_SEPARATOR = System.getProperty("line.separator");
		
		if(HELP) {
			builder.append("help para ajuda ou ").append(LINE_SEPARATOR);
			builder.append("nomeDestino <opcoes>").append(LINE_SEPARATOR);
			builder.append("Opcoes do programa:").append(LINE_SEPARATOR);
			builder.append("-FCS 	Fechar Consumidor").append(LINE_SEPARATOR);
			builder.append("-FCX 	Fechar Conexao").append(LINE_SEPARATOR);
			builder.append("-FT  	Fechar Tudo").append(LINE_SEPARATOR);
			builder.append("-FP  	Fechar Produtor").append(LINE_SEPARATOR);
			builder.append("-EM  	Enviar mensagem => Uso: EM=[mensagem]").append(LINE_SEPARATOR);
			builder.append("-QTD  	Quantidade de mensagens a serem enviadas => Uso: QTD=[quantidade]").append(LINE_SEPARATOR);
			builder.append("-IC  	Iniciar consumidor").append(LINE_SEPARATOR);
			builder.append("-DM  	Exibir detalhes da mensagem (cabeçalho)").append(LINE_SEPARATOR);
			builder.append("-NDM  	Não exibir detalhes da mensagem (cabeçalho)").append(LINE_SEPARATOR);
			builder.append("EX  	Sair do programa").append(LINE_SEPARATOR);
			builder.append("Digitar opcao desejada:").append(LINE_SEPARATOR);
			builder.append("------------------------").append(LINE_SEPARATOR);			
		} else {
			builder.append(">>>");
		}
		HELP = false;
		System.out.println(builder.toString());
	}
	
	public static void receberMensagemTexto(final JMSConnection jmsConnection, final String destino) throws JMSException {
		jmsConnection.receberMensagem(new MessageListener(){
			public void onMessage(Message message) {
				TextMessage textMessage = (TextMessage) message;
				String fila = destino;
				try {
					System.out.println("	CONSUMIDOR=>Recebendo Mensagem da fila "+fila);
					if(JMSConnection.isDetailedMessage()) {
						System.out.println("========  HEADER ========");
						
						System.out.println(" DeliveryMode="+jmsConnection.getDeliveryMode(message));
						System.out.println(" CorrelationID="+message.getJMSCorrelationID());
						System.out.println(" ExpirationTime="+jmsConnection.getTime(message.getJMSExpiration()));
						System.out.println(" MessageID="+message.getJMSMessageID());
						System.out.println(" Priority="+message.getJMSPriority());
						System.out.println(" Timestamp(Producer Sent)="+jmsConnection.getTime(message.getJMSTimestamp()));
						System.out.println(" Destination="+message.getJMSDestination());
						System.out.println(" Redelivered="+message.getJMSRedelivered());
						System.out.println(" ReplyTo="+message.getJMSReplyTo());
						
						System.out.println("========  HEADER ========");
					}
					System.out.println("	Mensagem: "+textMessage.getText());
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}}, true);
	}
	
	public static void enviarMensagemTexto(JMSConnection jmsConnection, final String texto) throws JMSException {
		ProdutorBuilder<TextMessage> produtor = jmsConnection.criarTextMessage();
		produtor.getMessage().setText(texto);
		produtor.enviar();
		System.out.println("Mensagem de texto enviada: "+texto);
	}
}
