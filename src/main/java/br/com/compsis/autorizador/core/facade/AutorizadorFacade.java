/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/01/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador.core.facade;

import java.util.List;

import br.com.compsis.autorizador.Util;
import br.com.compsis.autorizador.core.dao.sql.MOPDAO;
import br.com.compsis.autorizador.core.dao.sql.MidiaDAO;
import br.com.compsis.autorizador.core.dao.sql.SQLiteConnection;
import br.com.compsis.autorizador.domain.Autorizador;
import br.com.compsis.autorizador.domain.MOP;
import br.com.compsis.autorizador.domain.Midia;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/01/2015 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class AutorizadorFacade {
	private Autorizador autorizador;
	private MOPDAO mopDAO;
	private MidiaDAO midiaDAO;
	/** 
	 * TODO Construtor padrão/alternativo da classe
	 */
	public AutorizadorFacade(Autorizador autorizador) {
		this.autorizador = autorizador;
		SQLiteConnection conexao = Util.getConexao(autorizador); 
		this.mopDAO = new MOPDAO(conexao);
		this.midiaDAO = new MidiaDAO(conexao);
	}
	
	
	public List<MOP> carregarMOPsDoBanco() {
		return mopDAO.carregarTodosMOPs();
	}
	
	public synchronized void atualizarSequenciais(MOP mop) {
		mopDAO.atualizarSequenciais(mop);
	}
	
	public void complementarAutorizador() {
		autorizador.carregarTodosMOPs(carregarMOPsDoBanco());
	}
	
	public int atualizarMidia(Midia midia) {
		return midiaDAO.update(midia);
	}
	
	public void inserirMidia(Midia midia) {
		midiaDAO.insert(midia);
	}
}
