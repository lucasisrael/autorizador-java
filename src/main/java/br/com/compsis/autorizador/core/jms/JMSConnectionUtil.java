/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/12/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador.core.jms;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Session;

import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.api.jms.HornetQJMSClient;
import org.hornetq.api.jms.JMSFactoryType;
import org.hornetq.jms.client.HornetQJMSConnectionFactory;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/12/2014 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class JMSConnectionUtil {
	/**
	 * Porta padrao para conexao JMS
	 */
	public static Integer PORTA_PADRAO = 5445;
	private final static Map<String, ConnectionFactory> CFF_CACHE = Collections.synchronizedMap(new HashMap<String, ConnectionFactory>());
	public final static Map<String, JMSConnection> JMS_CONNECTIONS = Collections.synchronizedMap(new HashMap<String, JMSConnection>());
	
	public synchronized static ConnectionFactory getConnectionFactory(final String host, final int port) {
		String keyCFF = host+port+"_CF";
		ConnectionFactory connectionFactory = CFF_CACHE.get(keyCFF);
		if(connectionFactory==null) {
			Map<String, Object> aParams = new HashMap<String, Object>();
			aParams.put("host", host);
			aParams.put("port", port);
			connectionFactory = createConnectionFactory(aParams);
			CFF_CACHE.put(keyCFF, connectionFactory);
		}
		
		return connectionFactory;
	}
	
	public static Connection criarConexao(final String host) throws JMSException {
		ConnectionFactory connectionFactory = JMSConnectionUtil.getConnectionFactory(host, JMSConnectionUtil.PORTA_PADRAO);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		return connection;
	}
	
	public static Session criarSessao(final String host) throws JMSException {
		Connection conexao = criarConexao(host);
		Session session = conexao.createSession(false, Session.AUTO_ACKNOWLEDGE);
		return session;
	}
	
	public static JMSConnection criarJMSConnection(final String host, final String destino, final boolean isTopic) throws JMSException {
		String chave = host + destino;
		synchronized (JMS_CONNECTIONS) {
			JMSConnection jmsConnection = JMS_CONNECTIONS.get(chave);
			if(jmsConnection==null) {
				jmsConnection = new JMSConnection();
				jmsConnection.setChavePoolObjeto(chave);
				jmsConnection.setConnection(criarConexao(host));
				jmsConnection.setDestinationName(destino, isTopic); 			
			}
			return jmsConnection;			
		}
	}
	
	private static ConnectionFactory createConnectionFactory(final Map<String, Object> aParams) {
		HornetQJMSConnectionFactory connectionFactory = (HornetQJMSConnectionFactory) HornetQJMSClient . createConnectionFactoryWithoutHA
				( JMSFactoryType.CF , new TransportConfiguration( "org.hornetq.core.remoting.impl.netty.NettyConnectorFactory" , aParams )  );
		connectionFactory.setClientFailureCheckPeriod(1000);
		connectionFactory.setInitialConnectAttempts(2);
		connectionFactory.setRetryInterval(5);
		connectionFactory.setReconnectAttempts(2);
		connectionFactory.setProducerMaxRate(-1);
		connectionFactory.setProducerWindowSize(-1);
		connectionFactory.setConsumerMaxRate(-1);
		connectionFactory.setConsumerWindowSize(-1);
		connectionFactory.setConfirmationWindowSize(-1);
		return connectionFactory;
	}
}
