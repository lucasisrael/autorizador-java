/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/12/2014<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador.core.jms;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.hornetq.api.jms.HornetQJMSClient;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/12/2014 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@SuppressWarnings("unchecked")
public class JMSConnection {
	private Connection connection;
	private Session session;
	private Destination destination;
	private MessageProducer producer;
	private MessageConsumer consumer;
	private String chavePoolObjeto;
	private static boolean detailedMessage = false;
	
	/**
	 * Indica aos consumidores que devem coletar e imprimir os dados detalhado da mensagem. <br>
	 * Isto fará com que os cabeçalhos sejam impressos
	 */
	public static void showDetailedMessage() {
		detailedMessage = true;
	}
	
	/**
	 * Método de recuperação do campo detailedMessage <br>
	 * Esta flag indica aos consumidores que devem imprimir as informações de cabeçalho das mensagens
	 * @return valor do campo detailedMessage
	 */
	public static boolean isDetailedMessage() {
		return detailedMessage;
	}
	
	/**
	 * Indica aos consumidores que devem imprimir somente a mensagem que receber <br>
	 * Isto fará com que os dados do cabeçalho não sejam impressos
	 */
	public static void showShortMessage() {
		detailedMessage = false;
	}
	
	public <T extends Topic> T getTopic() {
		return (T) destination;
	}
	
	public <Q extends Queue> Q getQueue() {
		return (Q) destination;
	}
	
	/**
	 * Método de recuperação do campo connection
	 *
	 * @return valor do campo connection
	 */
	public Connection getConnection() {
		return connection;
	}
	/**
	 * Valor de connection atribuído a connection
	 *
	 * @param connection Atributo da Classe
	 * @throws JMSException 
	 */
	public void setConnection(Connection connection) throws JMSException {
		this.connection = connection;
		this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); 
	}
	/**
	 * Método de recuperação do campo session
	 *
	 * @return valor do campo session
	 */
	public Session getSession() {
		return session;
	}
	/**
	 * Valor de session atribuído a session
	 *
	 * @param session Atributo da Classe
	 */
	public void setSession(Session session) {
		this.session = session;
	}
	/**
	 * Método de recuperação do campo destination
	 *
	 * @return valor do campo destination
	 */
	public Destination getDestination() {
		return destination;
	}
	
	public void setDestinationName(final String destinationName, boolean isTopic) {
		destination = isTopic ? HornetQJMSClient.createTopic(destinationName) : HornetQJMSClient.createQueue(destinationName);
	}
	
	/**
	 * Valor de destination atribuído a destination
	 *
	 * @param destination Atributo da Classe
	 */
	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	/**
	 * Método de recuperação do campo producer
	 *
	 * @return valor do campo producer
	 * @throws JMSException 
	 */
	public MessageProducer getProducer() throws JMSException {
		if(producer == null) {
			producer = session.createProducer(destination);
		}
		return producer;
	}

	/**
	 * Valor de producer atribuído a producer
	 *
	 * @param producer Atributo da Classe
	 */
	public void setProducer(MessageProducer producer) {
		this.producer = producer;
	}

	public void fecharTudo() throws JMSException {
		fecharConsumidor();
		fecharProdutor();
		fecharSessao();
		fecharConexao();
		
	}
	
	public void enviarMenssagem(final Message message) throws JMSException {
		getProducer().send(message);
	}
	
	public void fecharConexao() throws JMSException {
		if(connection!=null){
			synchronized (JMSConnectionUtil.JMS_CONNECTIONS) {
				JMSConnectionUtil.JMS_CONNECTIONS.remove(chavePoolObjeto);				
			}
			connection.close();
		}
	}
	
	public void fecharSessao() throws JMSException {
		if(session!=null) {
			session.close();
			session = null;
		}
	}
	
	public void fecharConsumidor() throws JMSException {
		if(consumer!=null) {
			consumer.close();
			consumer = null;
		}
	}
	
	public void fecharProdutor() throws JMSException {
		if(producer!=null){
			producer.close();
			producer = null;
		}
	}
	
	/**
	 * Método de recuperação do campo consumer
	 *
	 * @return valor do campo consumer
	 * @throws JMSException 
	 */
	public MessageConsumer getConsumer() throws JMSException {
		if(consumer==null) {
			consumer = connection.createSession(false, Session.AUTO_ACKNOWLEDGE).createConsumer(getDestination());
		}
		return consumer;
	}
	
	public void receberMensagem(final MessageListener messageListener, final boolean isRecursive) throws JMSException {
		if(isRecursive) {
			getConsumer().setMessageListener(messageListener);
		} else {
			Message message = getConsumer().receive(1000);
			if(message !=null) {
				messageListener.onMessage(message);				
			}
		}
	}
	
	public ProdutorBuilder<TextMessage> criarTextMessage() throws JMSException {
		ProdutorBuilder<TextMessage> builder = new ProdutorBuilder<TextMessage>();
		builder.message = getSession().createTextMessage();
		return builder;
	}
	
	public ProdutorBuilder<BytesMessage> criarByteMessage() throws JMSException {
		ProdutorBuilder<BytesMessage> builder = new ProdutorBuilder<BytesMessage>();
		builder.message = getSession().createBytesMessage();
		return builder;
	}
	
	public ProdutorBuilder<ObjectMessage> criarObjectMessage() throws JMSException {
		ProdutorBuilder<ObjectMessage> builder = new ProdutorBuilder<ObjectMessage>();
		builder.message = getSession().createObjectMessage();
		return builder;
	}
	
	public class  ProdutorBuilder<M extends Message> {
		private Message message;
		
		public M getMessage() {
			return (M) message;
		}
		
		public void enviar() throws JMSException {
			JMSConnection.this.enviarMenssagem(message);
		}
	}
	
	/**
	 * Retorna a String com a descrição do {@link DeliveryMode}
	 * @param message
	 * @return
	 * @throws JMSException
	 */
	public String getDeliveryMode(final Message message) throws JMSException {
		return message.getJMSDeliveryMode() == DeliveryMode.NON_PERSISTENT ? "NON_PERSISTENT" : "PERSISTENT";
	}
	
	/**
	 * Formata os milissegundos passados por parametro para data/hora no formato dd/MM/yyyy hh:mm:ss do timezone America/Sao_Paulo
	 * @param time
	 * @return
	 */
	public String getTime(final long time) {
		TimeZone timeZone = TimeZone.getTimeZone("America/Sao_Paulo");
		Calendar calendar = Calendar.getInstance(timeZone);
		calendar.setTimeInMillis(time);
		Date date = calendar.getTime();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return simpleDateFormat.format(date);
	}

	/** 
	 * TODO Descrição do Método
	 * @param chave
	 */
	public void setChavePoolObjeto(String chave) {
		this.chavePoolObjeto = chave;
	}
}
