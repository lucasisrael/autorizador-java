package br.com.compsis.autorizador.core.dao.sql;

import java.util.Random;

public class Main {
	static final int TOTAL_MIDIAS_GERADAS = 10000000;
	static final int BLOCO_COMMIT = 10000;
	static MidiaDAO dao;
	static SQLiteConnection connection;

	public static void main(String[] args) {
		connection = new SQLiteConnection();
		connection.conectar();
//		connection.iniciarTabelas();
		System.out.println("Commitando a cada "+BLOCO_COMMIT+" registros.");
		dao = new MidiaDAO(connection);

		gerarMidias();

		connection.closeConnection();

	}

	public static void gerarMidias() {
		long inicio = System.currentTimeMillis();
		for (int i = 1; i < TOTAL_MIDIAS_GERADAS; i++) {
//			Midia midia = new Midia();
//			midia.setAcaoPista(i % 5);
//			midia.setEmissor(i % 100);
//			midia.setEstado(i % 3);
//			midia.setNumeroTag(i);
//			midia.setOsa(i % 4);
//			midia.setPlaca(gerarPlaca(i));
//			midia.setPrivilegio(i % 15);
//			dao.insert(midia);
			
			if( ( i % BLOCO_COMMIT) == 0 ) {
				connection.commit();
			}
			
			switch (i) {
			case 100:
				long fim = System.currentTimeMillis();
				System.out.println(i+" midias inseridas - Tempo: " + (fim - inicio) + " milis");				
				break;
			case 10000:
				fim = System.currentTimeMillis();
				System.out.println(i+" midias inseridas - Tempo: " + (fim - inicio) + " milis");				
				break;
			case 100000:
				fim = System.currentTimeMillis();
				System.out.println(i+" midias inseridas - Tempo: " + (fim - inicio) + " milis");				
				break;
			case 1000000:
				fim = System.currentTimeMillis();
				System.out.println(i+" midias inseridas - Tempo: " + (fim - inicio) + " milis");				
				break;
			}
		}
		long fim = System.currentTimeMillis();
		System.out.println(TOTAL_MIDIAS_GERADAS+" midias inseridas - Tempo: " + (fim - inicio) + " milis");				
	}

	private static String gerarPlaca(int i) {
		String placa = RANDOM_STRING.nextString()
				+ padStart(String.valueOf(i % 9999), '0', 4);
		return placa;
	}

	static String padStart(String input, char oChar, int len) {
		for (int i = 0; i < len; i++) {
			input = oChar + input;
		}
		return input;
	}

	static RandomString RANDOM_STRING = new RandomString(3);

	public static class RandomString {

		private static final char[] symbols;

		static {
			StringBuilder tmp = new StringBuilder();
			for (char ch = '0'; ch <= '9'; ++ch)
				tmp.append(ch);
			for (char ch = 'a'; ch <= 'z'; ++ch)
				tmp.append(ch);
			symbols = tmp.toString().toCharArray();
		}

		private final Random random = new Random();

		private final char[] buf;

		public RandomString(int length) {
			if (length < 1)
				throw new IllegalArgumentException("length < 1: " + length);
			buf = new char[length];
		}

		public String nextString() {
			for (int idx = 0; idx < buf.length; ++idx)
				buf[idx] = symbols[random.nextInt(symbols.length)];
			return new String(buf);
		}
	}
}
