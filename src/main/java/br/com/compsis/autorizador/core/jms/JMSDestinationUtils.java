/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/01/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador.core.jms;

import com.google.common.base.Strings;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/01/2015 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public final class JMSDestinationUtils {
	private JMSDestinationUtils(){}
	
	/**
	 * Obtém o nome ou IP do servidor JMS da string <br>
	 * Segue o padrão "nomeFileOuTopico@URLServer:PORTA"
	 * @param jmsInformationString a string contendo a informação de conexao jms
	 * @return
	 */
	public static String getServerJMS(final String jmsInformationString) {
		if(!Strings.isNullOrEmpty(jmsInformationString)) {
			if(jmsInformationString.contains("@")){
				if(jmsInformationString.contains(":")) {
					return jmsInformationString.substring(jmsInformationString.indexOf("@")+1, jmsInformationString.indexOf(":"));								
				} else {
					return jmsInformationString.substring(jmsInformationString.indexOf("@")+1);				
				}
			}
		}
		
		return null;
	}
	
	
	/**
	 * Porta do servidor JMS <br>
	 * Segue o padrão "nomeFileOuTopico@URLServer:PORTA"
	 * @param jmsInformationString a string contendo a informação de conexao jms
	 * @return
	 */
	public static Integer getPortJMS(final String jmsInformationString) {
		if(!Strings.isNullOrEmpty(jmsInformationString) && jmsInformationString.contains(":")) {
			return new Integer(jmsInformationString.substring(jmsInformationString.indexOf(":")+1));
		}
		return null;		
	}
	
	

	/**
	 * Nome do destino(topic ou queue) JMS <br>
	 * Obtém a partir do parametro que segue o padrão "nomeFileOuTopico@URLServer:PORTA"
	 * @param jmsInformationString a string contendo a informação de conexao jms
	 * @return
	 */
	public static String getDestinationName(final String jmsInformationString) {
		if(!Strings.isNullOrEmpty(jmsInformationString) && jmsInformationString.contains("@")) {
			if(jmsInformationString.contains("\\")){
				return jmsInformationString.substring(jmsInformationString.indexOf("\\")+1, jmsInformationString.indexOf("@"));
			} else if(jmsInformationString.contains("/")){
				return jmsInformationString.substring(jmsInformationString.indexOf("/")+1, jmsInformationString.indexOf("@"));				
			} else {
				return jmsInformationString.substring(0, jmsInformationString.indexOf("@"));				
			}
		}
		return null;
	}
	

	/**
	 * 
	 * Tipo do destino(se é um topic ou um queue) JMS <br>
	 * Obtém a partir do parametro que segue o padrão "nomeFileOuTopico@URLServer:PORTA" <br>
	 * Se não for mencionado, o padrão é "queue"
	 * @param jmsInformationString a string contendo a informação de conexao jms
	 * @return
	 */
	public static String getDestinationTypeJMS(final String jmsInformationString) {
		if(!Strings.isNullOrEmpty(jmsInformationString) && jmsInformationString.contains("@")) {
			if(jmsInformationString.contains("\\")){
				return jmsInformationString.substring(0, jmsInformationString.indexOf("\\"));
			} else if(jmsInformationString.contains("/")){
				return jmsInformationString.substring(0, jmsInformationString.indexOf("/"));				
			}
		}
		return "queue";
	}
	
}
