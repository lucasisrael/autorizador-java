/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/01/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador.core.jmx;

import br.com.compsis.autorizador.domain.Autorizador;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/01/2015 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class AutorizadorMBean implements IAutorizadorMBean{

	private Autorizador autorizador;

	/** 
	 * TODO Construtor padrão/alternativo da classe
	 * @param autorizador
	 */
	public AutorizadorMBean(Autorizador autorizador) {
		this.autorizador = autorizador;
	}

	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see br.com.compsis.autorizador.core.jmx.IAutorizadorMBean#getPraca()
	 */
	@Override
	public Integer getPraca() {
		return autorizador.getPraca();
	}

	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see br.com.compsis.autorizador.core.jmx.IAutorizadorMBean#getPista()
	 */
	@Override
	public Integer getPista() {
		return autorizador.getPista();
	}

	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see br.com.compsis.autorizador.core.jmx.IAutorizadorMBean#getCodigo()
	 */
	@Override
	public String getCodigo() {
		return autorizador.getCodigo();
	}

	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see br.com.compsis.autorizador.core.jmx.IAutorizadorMBean#getFilaSequencialTags()
	 */
	@Override
	public String getFilaSequencialTags() {
		return autorizador.getFilaSequencialTags();
	}

	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see br.com.compsis.autorizador.core.jmx.IAutorizadorMBean#getFilaTags()
	 */
	@Override
	public String getFilaTags() {
		return autorizador.getFilaTags();
	}

	
}
