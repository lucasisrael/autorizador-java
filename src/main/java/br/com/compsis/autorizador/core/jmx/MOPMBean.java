/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/01/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador.core.jmx;

import br.com.compsis.autorizador.Util;
import br.com.compsis.autorizador.core.facade.AutorizadorFacade;
import br.com.compsis.autorizador.domain.Autorizador;
import br.com.compsis.autorizador.domain.MOP;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/01/2015 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class MOPMBean implements IMOPMBean {
	private MOP mop;
	private AutorizadorFacade facade;
	
	/** 
	 * TODO Construtor padrão/alternativo da classe
	 */
	public MOPMBean(Autorizador autorizador, MOP mop) {
		this.mop = mop;
		facade = Util.getAutorizadorFacade(autorizador);
	}

	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see br.com.compsis.autorizador.core.jmx.IMOPMBean#getDescricao()
	 */
	@Override
	public String getDescricao() {
		return this.mop.getDescricao();
	}

	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see br.com.compsis.autorizador.core.jmx.IMOPMBean#getIdMeioPagamento()
	 */
	@Override
	public Integer getIdMeioPagamento() {
		return Integer.valueOf(this.mop.getCodigo());
	}

	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see br.com.compsis.autorizador.core.jmx.IMOPMBean#getOsaId()
	 */
	@Override
	public Integer getOsaId() {
		return this.mop.getOsaId();
	}

	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see br.com.compsis.autorizador.core.jmx.IMOPMBean#getSerie()
	 */
	@Override
	public Integer getSerie() {
		return this.mop.getSerie();
	}

	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see br.com.compsis.autorizador.core.jmx.IMOPMBean#getSequencial()
	 */
	@Override
	public Long getSequencial() {
		return this.mop.getSequencial();
	}

	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see br.com.compsis.autorizador.core.jmx.IMOPMBean#getGestor()
	 */
	@Override
	public Integer getGestor() {
		return this.mop.getIdGestor();
	}

	/** 
	 * TODO Descrição do Método
	 * @param sequencial
	 * @see br.com.compsis.autorizador.core.jmx.IMOPMBean#alterarSequencialPara(java.lang.Long)
	 */
	@Override
	public void alterarSequencialPara(Long sequencial) {
		Util.atualizarClassLoader();
		mop.setSequencial(sequencial);
		facade.atualizarSequenciais(mop);
	}

	/** 
	 * TODO Descrição do Método
	 * @param serie
	 * @see br.com.compsis.autorizador.core.jmx.IMOPMBean#alterarSeriePara(java.lang.Integer)
	 */
	@Override
	public void alterarSeriePara(Integer serie) {
		Util.atualizarClassLoader();
		mop.setSerie(serie);
		facade.atualizarSequenciais(mop);
	}
}
