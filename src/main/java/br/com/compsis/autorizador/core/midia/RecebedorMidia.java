/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/01/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador.core.midia;

import javax.jms.BytesMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vonbraunlabs.novoprotocolo.mensagemproto.SequencialTagProto.SequencialTag;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.Tag;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.TagEntrada;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.TagEntrada.Grupo;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.TagEntrada.PracaEntrada;
import org.vonbraunlabs.novoprotocolo.mensagemproto.TagProto.TagEntrada.Situacao;

import br.com.compsis.autorizador.Conexoes;
import br.com.compsis.autorizador.Util;
import br.com.compsis.autorizador.core.facade.AutorizadorFacade;
import br.com.compsis.autorizador.core.jms.JMSConnection;
import br.com.compsis.autorizador.core.jms.JMSConnectionUtil;
import br.com.compsis.autorizador.core.jms.JMSDestinationUtils;
import br.com.compsis.autorizador.domain.Autorizador;
import br.com.compsis.autorizador.domain.MOP;
import br.com.compsis.autorizador.domain.Midia;
import br.com.compsis.autorizador.domain.TipoConversao;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/01/2015 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class RecebedorMidia {
	private static Logger log = LoggerFactory.getLogger(RecebedorMidia.class);
	private Autorizador autorizador;
	private AutorizadorFacade facade;
	
	/** 
	 * TODO Construtor padrão/alternativo da classe
	 * @param autorizador
	 */
	public RecebedorMidia(Autorizador autorizador) {
		this.autorizador = autorizador;
		facade = Util.getAutorizadorFacade(autorizador);
	}

	/** 
	 * TODO Descrição do Método
	 * @param tag
	 */
	public void processarTag(Tag tag) {
	long inicio = System.currentTimeMillis();

		MOP mop = autorizador.getMOP(tag.getOsaId());
		String comparacao = " para OSA: " + mop.getDescricao() + " - Esperado {Série: "+mop.getSerie()+", Sequencial: "+(mop.getSequencial()+1)+"}, Recebido {Série:"+tag.getSerie()+", Sequencial:"+tag.getSequencial()+"}";
		log.info(autorizador.getCodigo() + comparacao);
		
		if(isSerieValida(mop, tag.getSerie())) {
			if(isSequencialValido(mop, tag.getSequencial())) {
				processarMidia(mop, tag);
				mop.setSequencial(tag.getSequencial());
				facade.atualizarSequenciais(mop);
				log.info("Recebido "+tag.getTagCount()+" em "+(System.currentTimeMillis() - inicio)+" milis.");
			} else {
				requisitarSequencial(mop);
				log.warn(autorizador.getCodigo()+" QUEBRASEQUENCIAL - Sequencial incorreto. Requisitando novo sequencial e atualizando serie no banco para a OSA: "+ mop.getDescricao() +". Nova Serie: "+mop.getSerie());				
			}
		} else {
			log.warn(autorizador.getCodigo()+" QUEBRASEQUENCIAL - Descartando mensagem da OSA:"+ mop.getDescricao() +". Série incorreta!");
		}
	}
	
	
	/** 
	 * TODO Descrição do Método
	 * @param mop
	 * @param tag
	 */
	private void processarMidia(MOP mop, Tag tag) {
		for (TagEntrada tagEntrada : tag.getTagList()) {
			Midia midia = adaptarParaMidia(mop, tag, tagEntrada);
			int midiasAtualizadas = facade.atualizarMidia(midia);
			if(midiasAtualizadas==0) {
				facade.inserirMidia(midia);
			}
		}
	}
	
	

	/** 
	 * TODO Descrição do Método
	 * @param mop
	 * @param tag
	 * @param tagEntrada 
	 */
	private Midia adaptarParaMidia(MOP mop, Tag tag, TagEntrada tagEntrada) {
		Midia midia = new Midia();
		boolean isento = tagEntrada.getGrupo().equals(Grupo.ISENTO) || tagEntrada.getGrupo().equals(Grupo.ISENTO_ARTESP);
		boolean bloqueado = tagEntrada.getSituacao().equals(Situacao.BLOQUEADO);
		boolean imagem = !tagEntrada.getTemPassagem();
		
		midia.setNumeroTag(tagEntrada.getTagId()).setCategoria(tagEntrada.getCategoria().getNumber()).setIdGestor(mop.getIdGestor())
		.setPlaca(tagEntrada.getPlaca()).setSequencial((int) tag.getSequencial()).setTipoMidia(0)
		.setTipoMidia(tagEntrada.getPlano().getNumber()).setTipoUsuario(0)
		.setIdMeioPagamento(Integer.valueOf(mop.getCodigo()));
		
		Integer pracaOSA = Integer.parseInt(mop.toExternalValue(TipoConversao.PRACAS, autorizador.getPraca().toString()));
		for (PracaEntrada pracaEntrada : tagEntrada.getPracaList()) {
			if(pracaEntrada.getPraca() == pracaOSA.intValue()) {
				bloqueado = true;
			}
		}
		for (PracaEntrada pracaEntrada : tagEntrada.getPracaGrupoList()) {
			if(pracaEntrada.getPraca() == pracaOSA.intValue() && ( Grupo.ISENTO.equals(pracaEntrada.getGrupo()) || Grupo.ISENTO_ARTESP.equals(pracaEntrada.getGrupo())  )) {
				isento = true;
			}
		}
		imagem |= isento ;
		
		midia.setPrivilegio(isento ? 15 : 0).setIsento(isento ? 1 : 0)
		.setStatus(bloqueado ? 1 : 0).setCodigoProblema( bloqueado ? tagEntrada.getMotivoBloqueio().getNumber() : 0)
		.setImagem(imagem ? 1 : 0);
		
		return midia;
	}

	/** 
	 * TODO Descrição do Método
	 * @param mop
	 */
	private void requisitarSequencial(MOP mop) {
		SequencialTag sequencialTag = SequencialTag.newBuilder().setConcessionariaId(0).setOsaId(mop.getOsaId()).setUltimoSequencial( (mop.getSequencial()+1) ).build();
		
		String destino = JMSDestinationUtils.getDestinationName(autorizador.getFilaSequencialTags());
		String host = JMSDestinationUtils.getServerJMS(autorizador.getFilaSequencialTags());
		
		try {
			JMSConnection jmsConnection = JMSConnectionUtil.criarJMSConnection(host, destino, false);
			BytesMessage message = jmsConnection.getSession().createBytesMessage();
			byte[] bytes = sequencialTag.toByteArray();
			message.writeBytes(bytes);
			jmsConnection.enviarMenssagem(message);
			log.info("Mensagem enviada para "+autorizador.getFilaSequencialTags()+" - MENSAGEM: "+sequencialTag.toString());
		} catch (Exception e) {
			log.error(e.getMessage());
			log.debug(e.getMessage(), e);
			Conexoes.addAutorizador(autorizador);
		}
		
		mop.setSerie((mop.getSerie()+1));
		facade.atualizarSequenciais(mop);
	}

	public boolean isSerieValida(MOP mop, int serie) {
		return mop.getSerie().intValue() == serie;
	}
	
	public boolean isSequencialValido(MOP mop, long sequencial) {
		return ( mop.getSequencial().longValue() + 1 ) == sequencial;
	}
	
}
