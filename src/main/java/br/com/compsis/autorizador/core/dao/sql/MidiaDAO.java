package br.com.compsis.autorizador.core.dao.sql;

import br.com.compsis.autorizador.Util;
import br.com.compsis.autorizador.domain.Midia;

public class MidiaDAO {
	
	private SQLiteConnection connection;
	
	public MidiaDAO(SQLiteConnection con) {
		this.connection = con;
	}
	
	public void insert(Midia midia) {
		
		String sql = Util.concat("INSERT INTO MIDIAS (        " ,
				"NumeroTag,                  " ,
				"IdEmissor,                  " ,
				"IdGestor                  , " ,
				"IdMeioPagamento           , " ,
				"Categoria                 , " ,
				"Placa                     , " ,
				"Privilegio                , " ,
				"Status                    , " ,
				"cCodigoProblema           , " ,
				"Imagem                    , " ,
				"Isento                    , " ,
				"uliIDCadastroMeioPagamento, " ,
				"SaldoMidia                , " ,
				"TipoMidia                 , " ,
				"TipoUsuario)                " ,
				"VALUES (                    '" ,    
				midia.getNumeroTag(), "', " ,
				"'',  " , //idEmissor vazio
				midia.getIdGestor(), ", ",
				midia.getIdMeioPagamento(),  ", ",
				midia.getCategoria(),  ", '",
				midia.getPlaca(), "', " ,
				midia.getPrivilegio(),", ",
				midia.getStatus(),", ",
				midia.getCodigoProblema(),", ",
				midia.getImagem(),", ",
				(midia.getIsento().intValue() == 1 ? "'true'" : "'false'" ),  ", ",
				midia.getSequencial(),", ",
				"0, ",
				midia.getTipoMidia(),", ",
				midia.getTipoUsuario(), " )" 
				
				);
		connection.executarSQL(sql);
	
	}
	
	public int update(Midia midia) {
		String sql = Util.concat("UPDATE MIDIAS SET IDEMISSOR='', IDGESTOR=", midia.getIdGestor(), ", IDMEIOPAGAMENTO=",
				midia.getIdMeioPagamento(), ", CATEGORIA=", midia.getCategoria(), ", PLACA='",
				midia.getPlaca(), "', privilegio=", midia.getPrivilegio(), ", status=", midia.getStatus(),
				", cCodigoProblema=", midia.getCodigoProblema(), ", imagem=", midia.getImagem(), ", isento=",
				(midia.getIsento().intValue() == 1 ? "'true'" : "'false'") , ", uliIDCadastroMeioPagamento=", midia.getSequencial(), ", saldomidia=0, tipomidia=",
				midia.getTipoMidia(), ", tipousuario=", midia.getTipoUsuario(), " WHERE NUMEROTAG='", midia.getNumeroTag(), "'" );
		return connection.executarSQL(sql);
	}
	
	public void delete (Midia midia) {
		String sql = "";
		connection.executarSQL(sql);
	}
	
	public Midia consultarId(Integer id) {
		Midia midia = new Midia();
//		midia.setId(id);
//		String sql = autoSQL.selectFrom(midia);
//		ResultSet resultSet = connection.consultarSQL(sql);
//		
//		try {
//			midia.setAcaoPista(resultSet.getInt("ACAOPISTA"));
//			midia.setEmissor(resultSet.getInt("EMISSOR"));
//			midia.setEstado(resultSet.getInt("ESTADO"));
//			midia.setNumeroTag(resultSet.getInt("NUMEROTAG"));
//			midia.setOsa(resultSet.getInt("OSA"));
//			midia.setPlaca(resultSet.getString("PLACA"));
//			midia.setPrivilegio(resultSet.getInt("PRIVILEGIO"));
//			resultSet.close();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
		return midia;
	}
	
}
