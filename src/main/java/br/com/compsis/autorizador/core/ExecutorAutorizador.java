
package br.com.compsis.autorizador.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.compsis.autorizador.Conexoes;
import br.com.compsis.autorizador.Util;
import br.com.compsis.autorizador.core.facade.AutorizadorFacade;
import br.com.compsis.autorizador.core.jms.JMSConnection;
import br.com.compsis.autorizador.core.jms.JMSConnectionUtil;
import br.com.compsis.autorizador.core.jms.JMSDestinationUtils;
import br.com.compsis.autorizador.core.jms.TagConsumer;
import br.com.compsis.autorizador.core.jmx.RegistradorJMX;
import br.com.compsis.autorizador.domain.Autorizador;


public class ExecutorAutorizador implements Runnable{
	private static Logger log = LoggerFactory.getLogger(ExecutorAutorizador.class);
	private Autorizador autorizador;
	
	/** 
	 * TODO Construtor padrão/alternativo da classe
	 * @param autorizador2
	 */
	public ExecutorAutorizador(Autorizador autorizador2) {
		this.autorizador = autorizador2;
	}

	@Override
	public void run() {
		Thread.currentThread().setName("Thread do autorizador: "+this.autorizador.getCodigo());
		log.info("Inicializando "+autorizador.toString());
		carregarMOPs();
		registrarAutorizadorJMX();
		registrarConsumidor();
		log.info("Autorizador inicializado "+autorizador.toString());		
	}
	
	/** 
	 * TODO Descrição do Método
	 */
	private void carregarMOPs() {
		AutorizadorFacade facade = Util.getAutorizadorFacade(autorizador);
		facade.complementarAutorizador();
	}

	private void registrarConsumidor() {
		String destino = JMSDestinationUtils.getDestinationName(autorizador.getFilaTags());
		String host = JMSDestinationUtils.getServerJMS(autorizador.getFilaTags());
		try {
			JMSConnection jmsConnection = JMSConnectionUtil.criarJMSConnection(host, destino, false);
			jmsConnection.receberMensagem(new TagConsumer(autorizador), true);
		} catch (Exception e) {
			log.error(e.getMessage());
			log.debug(e.getMessage(), e);
			Conexoes.addAutorizador(autorizador);
		}
	}

	private void registrarAutorizadorJMX() {
		RegistradorJMX registradorJMX = Util.getBean(RegistradorJMX.class);
		registradorJMX.registrarAutorizador(autorizador);
	}
}
