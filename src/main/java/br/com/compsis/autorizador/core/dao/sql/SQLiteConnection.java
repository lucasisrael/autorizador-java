package br.com.compsis.autorizador.core.dao.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SQLiteConnection {
	private static Logger log = LoggerFactory.getLogger(SQLiteConnection.class);
	private Connection c = null;
	private String arquivoDB;
	private boolean conectado;
	
	public SQLiteConnection conectar() {
		if(!conectado) {
			try {
				Class.forName("org.sqlite.JDBC");
				String url = "jdbc:sqlite:"+ (arquivoDB==null? "test.db": arquivoDB);
				log.info("Conectando. URL: "+url);
				c = DriverManager.getConnection(url);
				c.setAutoCommit(false);
				
				conectado = true;
				log.info("Conexao efetuada com sucesso! URL: "+url);
			} catch ( Exception e ) {
				log.error(e.getMessage());
				log.debug(e.getMessage(), e);
			}			
		}
	    return this;
	}

	/**
	 * Valor de arquivoDB atribuído a arquivoDB
	 *
	 * @param arquivoDB Atributo da Classe
	 */
	public SQLiteConnection setArquivoDB(String arquivoDB) {
		this.arquivoDB = arquivoDB;
		return this;
	}
	
	
	
	public int executarSQL(String sql) {
		Statement stmt = null;
		int registrosAtualizados = 0;
		try {
			
			stmt = c.createStatement(); 
			registrosAtualizados = stmt.executeUpdate(sql);
			stmt.close();
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		return registrosAtualizados;
	}
	
	public void commit() {
		try {
			c.commit();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public PreparedStatement createPreparedStatement(String sql) {
		PreparedStatement stmt = null;
		try {
			stmt = c.prepareStatement(sql); 
			return stmt;
		} catch ( Exception e ) {
			throw new RuntimeException(e);
		}
	}
	
	public ResultSet consultarSQL(String sql) {
		Statement stmt = null;
		try {
			
			stmt = c.createStatement(); 
			ResultSet resultSet = stmt.executeQuery(sql);
			return resultSet;
		} catch ( Exception e ) {
			throw new RuntimeException(e);
		}
	}
	
	public SQLiteConnection closeConnection() {
		if(conectado) {
			try {
				c.close();
				conectado = false;
			} catch (SQLException e) {
				log.error(e.getMessage());
				log.debug(e.getMessage(), e);
			}			
		}
		return this;
	}
	
	/**
	 * Método de recuperação do campo conectado
	 *
	 * @return valor do campo conectado
	 */
	public boolean isConectado() {
		return conectado;
	}
}
