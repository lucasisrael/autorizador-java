/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/01/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador.core.dao.sql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.compsis.autorizador.domain.MOP;
import br.com.compsis.autorizador.domain.TipoConversao;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/01/2015 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class MOPDAO {
	private static Logger log = LoggerFactory.getLogger(MOPDAO.class);
	private SQLiteConnection connection;
	/** 
	 * TODO Construtor padrão/alternativo da classe
	 */
	public MOPDAO(SQLiteConnection connection) {
		this.connection = connection;
	}
	
	public List<MOP> carregarTodosMOPs() {
		List<MOP> mops = new ArrayList<MOP>();
		String sql = "SELECT * FROM CONFIG_OSA_MP";
		
		ResultSet resultSet = connection.consultarSQL(sql);
		try {
			while(resultSet.next()) {
				MOP mop = new MOP();
				mop.setCodigo(String.valueOf(resultSet.getInt("IdMeioPagamento")));
				mop.setDescricao(resultSet.getString("Descricao"));
				mop.setOsaId(resultSet.getInt("IdOsa"));
				mop.setIdGestor(resultSet.getInt("IdGestor"));
				mops.add(mop);
			}
			resultSet.close();
		} catch (SQLException e) {
			log.error(e.getMessage());
			log.debug(e.getMessage(), e);
		}
		for (MOP mop : mops) {
			complementarSequencial(mop);
			complementarConversoes(mop);
		}
		return mops;
	}
	
	public void complementarSequencial(MOP mop) {
		String sql = "SELECT * FROM CONFIG_UPDATE WHERE Osa="+mop.getOsaId();
		
		ResultSet resultSet = connection.consultarSQL(sql);
		try {
			if(resultSet.next()) {
				mop.setSequencial(resultSet.getLong("Sequencial"));
				mop.setSerie(resultSet.getInt("Serie"));
			}
			resultSet.close();
		} catch (SQLException e) {
			log.error(e.getMessage());
			log.debug(e.getMessage(), e);
		}
	}
	
	public void complementarConversoes(MOP mop) {
		String sql = "SELECT * FROM CONFIG_OSA_PRACA WHERE IdGestor="+mop.getIdGestor();
		
		ResultSet resultSet = connection.consultarSQL(sql);
		try {
			while (resultSet.next()) {
				int IdPracaOsa = resultSet.getInt("IdPracaOsa");
				int IdPracaLocal = resultSet.getInt("IdPraca");
				mop.registrarConversao(TipoConversao.PRACAS, String.valueOf(IdPracaLocal), String.valueOf(IdPracaOsa));
			}
			resultSet.close();
		} catch (SQLException e) {
			log.error(e.getMessage());
			log.debug(e.getMessage(), e);
		}
	}
	
	
	public void atualizarSequenciais(MOP mop) {
		String sql = "UPDATE CONFIG_UPDATE SET Serie="+mop.getSerie()+", Sequencial="+mop.getSequencial()+" WHERE Osa="+mop.getOsaId();
		connection.executarSQL(sql);
		connection.commit();
	}
}
