
package br.com.compsis.autorizador;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.compsis.autorizador.core.ExecutorAutorizador;
import br.com.compsis.autorizador.core.dao.xmls.ConfiguracaoAutorizadorDAO;
import br.com.compsis.autorizador.domain.Autorizador;
import br.com.compsis.autorizador.domain.ConfiguracaoAutorizador;



public class Main {
	private static Logger log = LoggerFactory.getLogger(Main.class);
	public static void main(String[] args) {
		ConfiguracaoAutorizador configuracoes = carregarConfiguracoes();
		for (Autorizador autorizador : configuracoes.getAutorizadores()) {			
			ExecutorAutorizador executorAutorizador = new ExecutorAutorizador(autorizador);
			new Thread(executorAutorizador).start();
		}
		while(true) {
			try {				
				Thread.sleep(1000);
				for (Autorizador autorizador : configuracoes.getAutorizadores()) {
					if(Conexoes.getAutorizador(autorizador.getCodigo()) != null){
						ExecutorAutorizador executorAutorizador = new ExecutorAutorizador(autorizador);
						new Thread(executorAutorizador).start();
						Conexoes.delAutorizador(autorizador.getCodigo());
					}
				}
			} catch (InterruptedException e) {
				log.error("Morreu >>>>>>>>>>>>>>>>> ", e);
			}
		}
	}
	
	
	public static ConfiguracaoAutorizador carregarConfiguracoes() {
		ConfiguracaoAutorizadorDAO autorizadorDAO = Util.getBean(ConfiguracaoAutorizadorDAO.class);
		ConfiguracaoAutorizador configuracaoAutorizador = autorizadorDAO.carregarConfiguracaoAutorizador();
		if(configuracaoAutorizador == null){
			configuracaoAutorizador = new ConfiguracaoAutorizador();
			configuracaoAutorizador.setAutorizadores(new ArrayList<Autorizador>());
			Autorizador autorizador = new Autorizador();
			autorizador.setFilaSequencialTags("seqTags.PR012V05@CONCENTRADOR:5445");
			autorizador.setFilaTags("tags.PR012V05@CONCENTRADOR:5445");
			autorizador.setPraca(12);
			autorizador.setPista(5);			
			configuracaoAutorizador.getAutorizadores().add(autorizador);
			autorizadorDAO.escreverConfiguracaoAutorizador(configuracaoAutorizador);
			log.info("Criado nova ConfiguracaoAutorizador de exemplo");
			System.exit(0);
		}
		Util.registrarBean(ConfiguracaoAutorizador.class.getName(), configuracaoAutorizador);
		return configuracaoAutorizador;
	}
}
