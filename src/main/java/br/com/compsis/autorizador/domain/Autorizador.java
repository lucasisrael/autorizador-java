package br.com.compsis.autorizador.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Strings;

import br.com.compsis.autorizador.Util;

/**
 * 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/01/2015 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
public class Autorizador {
	private Integer praca;
	private Integer pista;
	private String filaTags;
	private String filaSequencialTags;
	
	private transient Map<Integer, MOP> mops = Collections.synchronizedMap(new HashMap<Integer, MOP>());
	
	/**
	 * Método de recuperação do campo praca
	 *
	 * @return valor do campo praca
	 */
	public Integer getPraca() {
		return praca;
	}
	/**
	 * Valor de praca atribuído a praca
	 *
	 * @param praca Atributo da Classe
	 */
	public void setPraca(Integer praca) {
		this.praca = praca;
	}
	/**
	 * Método de recuperação do campo pista
	 *
	 * @return valor do campo pista
	 */
	public Integer getPista() {
		return pista;
	}
	/**
	 * Valor de pista atribuído a pista
	 *
	 * @param pista Atributo da Classe
	 */
	public void setPista(Integer pista) {
		this.pista = pista;
	}
	/**
	 * Método de recuperação do campo filaTags
	 *
	 * @return valor do campo filaTags
	 */
	public String getFilaTags() {
		return filaTags;
	}
	/**
	 * Valor de filaTags atribuído a filaTags
	 *
	 * @param filaTags Atributo da Classe
	 */
	public void setFilaTags(String filaTags) {
		this.filaTags = filaTags;
	}
	/**
	 * Método de recuperação do campo filaSequencialTags
	 *
	 * @return valor do campo filaSequencialTags
	 */
	public String getFilaSequencialTags() {
		return filaSequencialTags;
	}
	/**
	 * Valor de filaSequencialTags atribuído a filaSequencialTags
	 *
	 * @param filaSequencialTags Atributo da Classe
	 */
	public void setFilaSequencialTags(String filaSequencialTags) {
		this.filaSequencialTags = filaSequencialTags;
	}
	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		synchronized (this.mops) {
			return "Autorizador [praca=" + praca + ", pista=" + pista
					+ ", filaTags=" + filaTags + ", filaSequencialTags="
					+ filaSequencialTags + "]";			
		}
	}
	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		synchronized (this.mops) {
			final int prime = 31;
			int result = 1;
			result = prime
					* result
					+ ((filaSequencialTags == null) ? 0 : filaSequencialTags
							.hashCode());
			result = prime * result
					+ ((filaTags == null) ? 0 : filaTags.hashCode());
			result = prime * result + ((pista == null) ? 0 : pista.hashCode());
			result = prime * result + ((praca == null) ? 0 : praca.hashCode());
			return result;			
		}
	}
	/** 
	 * TODO Descrição do Método
	 * @param obj
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		synchronized (this.mops) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Autorizador other = (Autorizador) obj;
			if (filaSequencialTags == null) {
				if (other.filaSequencialTags != null)
					return false;
			} else if (!filaSequencialTags.equals(other.filaSequencialTags))
				return false;
			if (filaTags == null) {
				if (other.filaTags != null)
					return false;
			} else if (!filaTags.equals(other.filaTags))
				return false;
			if (pista == null) {
				if (other.pista != null)
					return false;
			} else if (!pista.equals(other.pista))
				return false;
			if (praca == null) {
				if (other.praca != null)
					return false;
			} else if (!praca.equals(other.praca))
				return false;
			return true;			
		}
	}
	
	
	public void putMOP(MOP mop) {
		synchronized (this.mops) {
			mops.put(mop.getOsaId(), mop);			
		}
	}
	
	public MOP getMOP(Integer osaId) {
		synchronized (this.mops) {
			return mops.get(osaId);			
		}
	}
	
	public List<MOP> listMOPs() {
		synchronized (this.mops) {
			return new ArrayList<MOP>(mops.values());			
		}
	}
	
	public String getCodigo() {
		return Util.concat("PR", Strings.padStart(String.valueOf(praca), 3, '0'), "V", Strings.padStart(String.valueOf(pista), 2, '0'));
	}
	/** 
	 * TODO Descrição do Método
	 * @param carregarMOPsDoBanco
	 */
	public void carregarTodosMOPs(List<MOP> carregarMOPsDoBanco) {
		synchronized (this.mops) {
			this.mops.clear();
			for (MOP mop : carregarMOPsDoBanco) {
				mops.put(mop.getOsaId(), mop);
			}			
		}
		
	}
}
