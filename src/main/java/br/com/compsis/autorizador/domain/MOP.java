/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/01/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador.domain;

import java.util.HashMap;
import java.util.Map;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/01/2015 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class MOP {
	private String codigo;
	private Integer osaId;
	private Integer idGestor;
	private String descricao;
	private Integer serie;
	private Long sequencial;
	private Map<TipoConversao, Map<String, String>> internoParaExterno = new HashMap<TipoConversao, Map<String,String>>();
	private Map<TipoConversao, Map<String, String>> externoParaInterno = new HashMap<TipoConversao, Map<String,String>>();
	
	/**
	 * Valor de idGestor atribuído a idGestor
	 *
	 * @param idGestor Atributo da Classe
	 */
	public void setIdGestor(Integer idGestor) {
		this.idGestor = idGestor;
	}
	
	/**
	 * Método de recuperação do campo idGestor
	 *
	 * @return valor do campo idGestor
	 */
	public Integer getIdGestor() {
		return idGestor;
	}
	
	/**
	 * Método de recuperação do campo codigo
	 *
	 * @return valor do campo codigo
	 */
	public String getCodigo() {
		return codigo;
	}
	/**
	 * Valor de codigo atribuído a codigo
	 *
	 * @param codigo Atributo da Classe
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	/**
	 * Método de recuperação do campo osaId
	 *
	 * @return valor do campo osaId
	 */
	public Integer getOsaId() {
		return osaId;
	}
	/**
	 * Valor de osaId atribuído a osaId
	 *
	 * @param osaId Atributo da Classe
	 */
	public void setOsaId(Integer osaId) {
		this.osaId = osaId;
	}
	/**
	 * Método de recuperação do campo descricao
	 *
	 * @return valor do campo descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * Valor de descricao atribuído a descricao
	 *
	 * @param descricao Atributo da Classe
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * Método de recuperação do campo serie
	 *
	 * @return valor do campo serie
	 */
	public Integer getSerie() {
		return serie;
	}
	/**
	 * Valor de serie atribuído a serie
	 *
	 * @param serie Atributo da Classe
	 */
	public void setSerie(Integer serie) {
		this.serie = serie;
	}
	/**
	 * Método de recuperação do campo sequencial
	 *
	 * @return valor do campo sequencial
	 */
	public Long getSequencial() {
		return sequencial;
	}
	/**
	 * Valor de sequencial atribuído a sequencial
	 *
	 * @param sequencial Atributo da Classe
	 */
	public void setSequencial(Long sequencial) {
		this.sequencial = sequencial;
	}
	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MOP [codigo=" + codigo + ", osaId=" + osaId + ", idGestor="
				+ idGestor + ", descricao=" + descricao + ", serie=" + serie
				+ ", sequencial=" + sequencial + ", internoParaExterno="
				+ internoParaExterno + ", externoParaInterno="
				+ externoParaInterno + "]";
	}
	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime
				* result
				+ ((externoParaInterno == null) ? 0 : externoParaInterno
						.hashCode());
		result = prime * result
				+ ((idGestor == null) ? 0 : idGestor.hashCode());
		result = prime
				* result
				+ ((internoParaExterno == null) ? 0 : internoParaExterno
						.hashCode());
		result = prime * result + ((osaId == null) ? 0 : osaId.hashCode());
		result = prime * result
				+ ((sequencial == null) ? 0 : sequencial.hashCode());
		result = prime * result + ((serie == null) ? 0 : serie.hashCode());
		return result;
	}
	/** 
	 * TODO Descrição do Método
	 * @param obj
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MOP other = (MOP) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (externoParaInterno == null) {
			if (other.externoParaInterno != null)
				return false;
		} else if (!externoParaInterno.equals(other.externoParaInterno))
			return false;
		if (idGestor == null) {
			if (other.idGestor != null)
				return false;
		} else if (!idGestor.equals(other.idGestor))
			return false;
		if (internoParaExterno == null) {
			if (other.internoParaExterno != null)
				return false;
		} else if (!internoParaExterno.equals(other.internoParaExterno))
			return false;
		if (osaId == null) {
			if (other.osaId != null)
				return false;
		} else if (!osaId.equals(other.osaId))
			return false;
		if (sequencial == null) {
			if (other.sequencial != null)
				return false;
		} else if (!sequencial.equals(other.sequencial))
			return false;
		if (serie == null) {
			if (other.serie != null)
				return false;
		} else if (!serie.equals(other.serie))
			return false;
		return true;
	}
	
	public void registrarConversao(TipoConversao tipo, String valorInterno, String valorExterno) {
		Map<String, String> extToInterno = externoParaInterno.get(tipo);
		if(extToInterno==null) {
			extToInterno = new HashMap<String, String>();
			externoParaInterno.put(tipo, extToInterno);
		}
		extToInterno.put(valorExterno, valorInterno);
		
		Map<String, String> internoToExt = internoParaExterno.get(tipo);
		if(internoToExt == null) {
			internoToExt = new HashMap<String, String>();
			internoParaExterno.put(tipo, internoToExt);
		}
		
		internoToExt.put(valorInterno, valorExterno);
	}
	
	public String toInternalValue(TipoConversao tipo, String valorExterno) {
		return externoParaInterno.get(tipo).get(valorExterno);
	}
	
	public String toExternalValue(TipoConversao tipo, String valorInterno) {
		return internoParaExterno.get(tipo).get(valorInterno);
	}
}
