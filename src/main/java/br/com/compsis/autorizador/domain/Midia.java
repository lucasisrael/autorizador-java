/*
 * COMPSIS – Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto ${product_name} - ${product_description}<br>
 *
 * Data de Criação: 24/01/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador.domain;

import com.google.common.base.Strings;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 24/01/2015 - @author Lucas Israel - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class Midia {
	/**
	 * NumeroTag                  TEXT,
    IdEmissor                  TEXT,
    IdGestor                   SMALLINT,
    IdMeioPagamento            SMALLINT,
    Categoria                  SMALLINT,
    Placa                      TEXT,
    Privilegio                 SMALLINT,
    Status                     SMALLINT,
    cCodigoProblema            SMALLINT,
    Imagem                     SMALLINT,
    Isento                     SMALLINT,
    uliIDCadastroMeioPagamento INTEGER,
    SaldoMidia                 INTEGER,
    TipoMidia                  SMALLINT,
    TipoUsuario                SMALLINT 
	 */
	private Integer id;
	private String numeroTag;
	private Integer idGestor;
	private Integer idMeioPagamento;
	private Integer categoria;
	private String placa;
	private Integer privilegio;
	private Integer status;
	private Integer codigoProblema;
	private Integer imagem;
	private Integer isento;
	private Integer sequencial;
	private Integer tipoMidia;
	private Integer tipoUsuario;
	/**
	 * Método de recuperação do campo id
	 *
	 * @return valor do campo id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * Valor de id atribuído a id
	 *
	 * @param id Atributo da Classe
	 */
	public Midia setId(Integer id) {
		this.id = id;
		return this;
	}
	/**
	 * Método de recuperação do campo numeroTag
	 *
	 * @return valor do campo numeroTag
	 */
	public String getNumeroTag() {
		return numeroTag;
	}
	/**
	 * Valor de numeroTag atribuído a numeroTag
	 *
	 * @param numeroTag Atributo da Classe
	 */
	public Midia setNumeroTag(String numeroTag) {
		this.numeroTag = numeroTag;
		return this;
	}
	/**
	 * Método de recuperação do campo idGestor
	 *
	 * @return valor do campo idGestor
	 */
	public Integer getIdGestor() {
		return idGestor;
	}
	/**
	 * Valor de idGestor atribuído a idGestor
	 *
	 * @param idGestor Atributo da Classe
	 */
	public Midia setIdGestor(Integer idGestor) {
		this.idGestor = idGestor;
		return this;
	}
	/**
	 * Método de recuperação do campo idMeioPagamento
	 *
	 * @return valor do campo idMeioPagamento
	 */
	public Integer getIdMeioPagamento() {
		return idMeioPagamento;
	}
	/**
	 * Valor de idMeioPagamento atribuído a idMeioPagamento
	 *
	 * @param idMeioPagamento Atributo da Classe
	 */
	public Midia setIdMeioPagamento(Integer idMeioPagamento) {
		this.idMeioPagamento = idMeioPagamento;
		return this;
	}
	/**
	 * Método de recuperação do campo categoria
	 *
	 * @return valor do campo categoria
	 */
	public Integer getCategoria() {
		return categoria;
	}
	/**
	 * Valor de categoria atribuído a categoria
	 *
	 * @param categoria Atributo da Classe
	 */
	public Midia setCategoria(Integer categoria) {
		this.categoria = categoria;
		return this;
	}
	/**
	 * Método de recuperação do campo placa
	 *
	 * @return valor do campo placa
	 */
	public String getPlaca() {
		return placa;
	}
	/**
	 * Valor de placa atribuído a placa
	 *
	 * @param placa Atributo da Classe
	 */
	public Midia setPlaca(String placa) {
		this.placa = placa;
		return this;
	}
	/**
	 * Método de recuperação do campo privilegio
	 *
	 * @return valor do campo privilegio
	 */
	public Integer getPrivilegio() {
		return privilegio;
	}
	/**
	 * Valor de privilegio atribuído a privilegio
	 *
	 * @param privilegio Atributo da Classe
	 */
	public Midia setPrivilegio(Integer privilegio) {
		this.privilegio = privilegio;
		return this;
	}
	/**
	 * Método de recuperação do campo status
	 *
	 * @return valor do campo status
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * Valor de status atribuído a status
	 *
	 * @param status Atributo da Classe
	 */
	public Midia setStatus(Integer status) {
		this.status = status;
		return this;
	}
	/**
	 * Método de recuperação do campo codigoProblema
	 *
	 * @return valor do campo codigoProblema
	 */
	public Integer getCodigoProblema() {
		return codigoProblema;
	}
	/**
	 * Valor de codigoProblema atribuído a codigoProblema
	 *
	 * @param codigoProblema Atributo da Classe
	 */
	public Midia setCodigoProblema(Integer codigoProblema) {
		this.codigoProblema = codigoProblema;
		return this;
	}
	/**
	 * Método de recuperação do campo imagem
	 *
	 * @return valor do campo imagem
	 */
	public Integer getImagem() {
		return imagem;
	}
	/**
	 * Valor de imagem atribuído a imagem
	 *
	 * @param imagem Atributo da Classe
	 */
	public Midia setImagem(Integer imagem) {
		this.imagem = imagem;
		return this;
	}
	/**
	 * Método de recuperação do campo isento
	 *
	 * @return valor do campo isento
	 */
	public Integer getIsento() {
		return isento;
	}
	/**
	 * Valor de isento atribuído a isento
	 *
	 * @param isento Atributo da Classe
	 */
	public Midia setIsento(Integer isento) {
		this.isento = isento;
		return this;
	}
	/**
	 * Método de recuperação do campo sequencial
	 *
	 * @return valor do campo sequencial
	 */
	public Integer getSequencial() {
		return sequencial;
	}
	/**
	 * Valor de sequencial atribuído a sequencial
	 *
	 * @param sequencial Atributo da Classe
	 */
	public Midia setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
		return this;
	}
	/**
	 * Método de recuperação do campo tipoMidia
	 *
	 * @return valor do campo tipoMidia
	 */
	public Integer getTipoMidia() {
		return tipoMidia;
	}
	/**
	 * Valor de tipoMidia atribuído a tipoMidia
	 *
	 * @param tipoMidia Atributo da Classe
	 */
	public Midia setTipoMidia(Integer tipoMidia) {
		this.tipoMidia = tipoMidia;
		return this;
	}
	/**
	 * Método de recuperação do campo tipoUsuario
	 *
	 * @return valor do campo tipoUsuario
	 */
	public Integer getTipoUsuario() {
		return tipoUsuario;
	}
	/**
	 * Valor de tipoUsuario atribuído a tipoUsuario
	 *
	 * @param tipoUsuario Atributo da Classe
	 */
	public Midia setTipoUsuario(Integer tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
		return this;
	}
	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Midia [id=" + id + ", numeroTag=" + numeroTag + ", idGestor="
				+ idGestor + ", idMeioPagamento=" + idMeioPagamento
				+ ", categoria=" + categoria + ", placa=" + placa
				+ ", privilegio=" + privilegio + ", status=" + status
				+ ", codigoProblema=" + codigoProblema + ", imagem=" + imagem
				+ ", isento=" + isento + ", sequencial=" + sequencial
				+ ", tipoMidia=" + tipoMidia + ", tipoUsuario=" + tipoUsuario
				+ "]";
	}
	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((categoria == null) ? 0 : categoria.hashCode());
		result = prime * result
				+ ((codigoProblema == null) ? 0 : codigoProblema.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((idGestor == null) ? 0 : idGestor.hashCode());
		result = prime * result
				+ ((idMeioPagamento == null) ? 0 : idMeioPagamento.hashCode());
		result = prime * result + ((imagem == null) ? 0 : imagem.hashCode());
		result = prime * result + ((isento == null) ? 0 : isento.hashCode());
		result = prime * result
				+ ((numeroTag == null) ? 0 : numeroTag.hashCode());
		result = prime * result + ((placa == null) ? 0 : placa.hashCode());
		result = prime * result
				+ ((privilegio == null) ? 0 : privilegio.hashCode());
		result = prime * result
				+ ((sequencial == null) ? 0 : sequencial.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result
				+ ((tipoMidia == null) ? 0 : tipoMidia.hashCode());
		result = prime * result
				+ ((tipoUsuario == null) ? 0 : tipoUsuario.hashCode());
		return result;
	}
	/** 
	 * TODO Descrição do Método
	 * @param obj
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Midia other = (Midia) obj;
		if (categoria == null) {
			if (other.categoria != null)
				return false;
		} else if (!categoria.equals(other.categoria))
			return false;
		if (codigoProblema == null) {
			if (other.codigoProblema != null)
				return false;
		} else if (!codigoProblema.equals(other.codigoProblema))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idGestor == null) {
			if (other.idGestor != null)
				return false;
		} else if (!idGestor.equals(other.idGestor))
			return false;
		if (idMeioPagamento == null) {
			if (other.idMeioPagamento != null)
				return false;
		} else if (!idMeioPagamento.equals(other.idMeioPagamento))
			return false;
		if (imagem == null) {
			if (other.imagem != null)
				return false;
		} else if (!imagem.equals(other.imagem))
			return false;
		if (isento == null) {
			if (other.isento != null)
				return false;
		} else if (!isento.equals(other.isento))
			return false;
		if (numeroTag == null) {
			if (other.numeroTag != null)
				return false;
		} else if (!numeroTag.equals(other.numeroTag))
			return false;
		if (placa == null) {
			if (other.placa != null)
				return false;
		} else if (!placa.equals(other.placa))
			return false;
		if (privilegio == null) {
			if (other.privilegio != null)
				return false;
		} else if (!privilegio.equals(other.privilegio))
			return false;
		if (sequencial == null) {
			if (other.sequencial != null)
				return false;
		} else if (!sequencial.equals(other.sequencial))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (tipoMidia == null) {
			if (other.tipoMidia != null)
				return false;
		} else if (!tipoMidia.equals(other.tipoMidia))
			return false;
		if (tipoUsuario == null) {
			if (other.tipoUsuario != null)
				return false;
		} else if (!tipoUsuario.equals(other.tipoUsuario))
			return false;
		return true;
	}
	/** 
	 * TODO Descrição do Método
	 * @param tagId
	 */
	public Midia setNumeroTag(long tagId) {
		this.numeroTag = Strings.padStart(String.valueOf(tagId), 16, '0');
		return this;
	}
	
	
}
