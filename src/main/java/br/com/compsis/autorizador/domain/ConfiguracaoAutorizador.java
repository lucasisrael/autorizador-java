
package br.com.compsis.autorizador.domain;

import java.util.List;


public class ConfiguracaoAutorizador {
	private List<Autorizador> autorizadores;

	/**
	 * Método de recuperação do campo autorizadores
	 *
	 * @return valor do campo autorizadores
	 */
	public List<Autorizador> getAutorizadores() {
		return autorizadores;
	}

	/**
	 * Valor de autorizadores atribuído a autorizadores
	 *
	 * @param autorizadores Atributo da Classe
	 */
	public void setAutorizadores(List<Autorizador> autorizadores) {
		this.autorizadores = autorizadores;
	}

	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ConfiguracaoAutorizador [autorizadores=" + autorizadores + "]";
	}

	/** 
	 * TODO Descrição do Método
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((autorizadores == null) ? 0 : autorizadores.hashCode());
		return result;
	}

	/** 
	 * TODO Descrição do Método
	 * @param obj
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConfiguracaoAutorizador other = (ConfiguracaoAutorizador) obj;
		if (autorizadores == null) {
			if (other.autorizadores != null)
				return false;
		} else if (!autorizadores.equals(other.autorizadores))
			return false;
		return true;
	}


}
