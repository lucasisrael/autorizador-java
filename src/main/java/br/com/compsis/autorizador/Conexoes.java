/*
 * COMPSIS � Computadores e Sistemas Ind. e Com. LTDA<br>
 * TODO Produto $(product_name} - ${product_description}<br>
 *
 * Data de Cria��o: 02/02/2015<br>
 * <br>
 * Todos os direitos reservados.
 */

package br.com.compsis.autorizador;

import java.util.HashMap;
import java.util.Map;

import br.com.compsis.autorizador.domain.Autorizador;

/** 
 * DOCUMENTA��O DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documenta��o da classe. <br>
 * <br>
 * HIST�RICO DE DESENVOLVIMENTO: <br>
 * 02/02/2015 - @author Daniel Ferraz - Primeira vers�o da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public class Conexoes {
	
	private static Map<String, Autorizador> conexoes = new HashMap<String, Autorizador>();
	
	private static Object sync = new Object();
	
	
	public static void addAutorizador(Autorizador autorizador){
		synchronized(sync){
			conexoes.put(autorizador.getCodigo(), autorizador);
		}
	}
	
	public static Autorizador getAutorizador(String codigo){
		synchronized(sync){
			return conexoes.get(codigo);
		}
	}

	public static void delAutorizador(String codigo){
		synchronized(sync){
			conexoes.remove(codigo);
		}
	}
	

}
